(** The Gemini transport protocol. *)

type t
(** A protocol configuration. *)

type response =
  | OK of { url : Uri.t; mime_type : string; data : string }

val create : _ Eio.Net.t -> t

val get : t -> Uri.t -> response
(** [get t uri] connects to [uri]'s server and requests [uri] from it. *)
