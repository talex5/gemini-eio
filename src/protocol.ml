open Eio.Std
open Astring

let src = Logs.Src.create "gemini-protocol" ~doc:"Gemini protocol library"
module Log = (val Logs.src_log src : Logs.LOG)

let tls_config =
  let null ?ip:_ ~host:_ _certs = Ok None in
  match Tls.Config.client ~authenticator:null () with      (* todo: TOFU *)
  | Ok x -> x
  | Error (`Msg m) -> failwith m

type t = {
  net : [`Generic] Eio.Net.ty r;
}

let create net =
  { net = (net :> [`Generic] Eio.Net.ty r) }

type response =
  | OK of { url : Uri.t; mime_type : string; data : string }

let response =
  let open Eio.Buf_read.Syntax in
  let* code = Eio.Buf_read.take 2 in
  match code.[0] with
  | '2' ->
    let+ mime_type = Eio.Buf_read.(char ' ' *> line)
    and+ body = Eio.Buf_read.take_all in
    `OK (mime_type, body)
  | '3' ->
    let+ uri = Eio.Buf_read.(char ' ' *> line) in
    `Redirect (Uri.of_string uri)
  | _ -> Fmt.failwith "Unsupported status code %S" code

let rec get t url =
  if Uri.scheme url <> Some "gemini" then Fmt.failwith "Not a gemini URL: %a" Uri.pp url;
  let port = Uri.port url |> Option.value ~default:1965 in
  match Uri.host url with
  | None -> Fmt.failwith "Missing host in URL %a" Uri.pp url
  | Some host ->
    let response =
      Eio.Net.with_tcp_connect t.net ~service:(string_of_int port) ~host @@ fun conn ->
      let host = Domain_name.of_string_exn host |> Domain_name.host |> Result.to_option in
      let conn = Tls_eio.client_of_flow tls_config ?host conn in
      Eio.Flow.copy_string (Uri.to_string url ^ "\r\n") conn;
      Eio.Buf_read.parse_exn response conn ~max_size:(1024 * 1024)
    in
    match response with
    | `OK (mime_type, data) ->
      Log.debug (fun f -> f "OK: mime-type=%S" mime_type);
      OK { url; mime_type; data }
    | `Redirect uri ->
      Log.debug (fun f -> f "Redirect to %a" Uri.pp uri);
      get t uri
