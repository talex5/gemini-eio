open Astring

type t = {
  base : Uri.t;
  mutable verbatim : bool;
}

type line = [
  | `Text of string
  | `Link of Uri.t * string option
  | `Verbatim of string
  | `Fence of string
  | `Blank
]

let create base =
  { base; verbatim = false }

let non_whitespace = function
  | ' ' | '\t' -> false
  | _ -> true

let parse_link ~base x =
  let x = String.trim x in
  let link, desc = String.span x ~sat:non_whitespace in
  let desc = String.trim desc in
  let desc = if desc = "" then None else Some desc in
  let url = Uri.resolve "gemini" base (Uri.of_string link) in
  `Link (url, desc)

let input t raw =
  match String.trim raw with
  | "" -> `Blank
  | line ->
    let after prefix =
      if String.is_prefix ~affix:prefix line then Some (String.with_range line ~first:(String.length prefix))
      else None
    in
    match after "```" with
    | Some x -> t.verbatim <- not t.verbatim; `Fence x
    | None ->
      if t.verbatim then `Verbatim raw
      else  (
        match after "=>" with
        | Some x -> parse_link ~base:t.base x
        | None -> `Text line
      )
