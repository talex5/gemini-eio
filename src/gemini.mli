(** The gemini data format. *)

type t
(** The state of a gemini parser. *)

type line = [
  | `Text of string
  | `Link of Uri.t * string option
  | `Verbatim of string
  | `Fence of string
  | `Blank
]

val create : Uri.t -> t
(** [create uri] is a new processor ready to input a gemini document from [uri]
    (which is used to resolve links). *)

val input : t -> string -> line
(** [input t line] parses [line] using and updating the state in [t]. *)
