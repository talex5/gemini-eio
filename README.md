# gemini-eio

This is a simple test case for OCaml's new [Eio](https://github.com/ocaml-multicore/eio) library for IO using multicore OCaml's support for effects.

You will need a version of OCaml with effects. You can get one like this:

```
opam switch create 5.0.0~beta1 --repo=default,alpha=git+https://github.com/kit-ty-kate/opam-alpha-repository.git
```

To install gemini-eio:

```
git clone --recursive https://gitlab.com/talex5/gemini-eio.git
cd gemini-eio
opam install .
```

## Command-line use

```
gemini-eio gemini://gemini.circumlunar.space/
```
